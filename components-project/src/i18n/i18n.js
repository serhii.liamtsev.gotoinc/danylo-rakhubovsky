import { languages, defaultLocale } from './index'
import { createI18n } from 'vue-i18n'

const localStorageLang = localStorage.getItem('lang')
const messages = Object.assign(languages)
export const i18n = createI18n({
  legacy: false,
  locale: localStorageLang || defaultLocale,
  fallbackLocale: 'en',
  messages
})
