import { createApp } from 'vue'
import router from './router'
import './css/font.css'
import './index.css'
import 'v-calendar/dist/style.css'
import App from './App.vue'
import VCalendar from 'v-calendar'

import { i18n } from './i18n/i18n'
createApp(App).use(router).use(VCalendar).use(i18n).mount('#app')
