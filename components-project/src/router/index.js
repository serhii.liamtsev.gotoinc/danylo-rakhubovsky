import { createRouter, createWebHistory } from 'vue-router'
import FormPage from '../pages/FormPage.vue'
import ComponentsPage from '../pages/ComponentsPage.vue'

const routes = [
  { path: '/', name: 'home', component: ComponentsPage },
  { path: '/form', name: 'form', component: FormPage }
]
const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
