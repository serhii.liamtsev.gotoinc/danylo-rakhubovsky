/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js}'],
  theme: {
    extend: {
      fontFamily: {
        'selected-option': ['Inter']
      },
      colors: {
        'select-border': '#E8EAF3',
        'selected-option': '#777B86',
        'select-is-open': '#dbc3fd',
        'active-option': 'rgb(182, 182, 226)',
        'active-option-slot': 'rgb(222, 100, 226)',
        'active-checkBox': '#A16CF6',
        accordion: '#e4d7fa',
        'accordion-hover': '#eae3f6',
        'form-label': '#6F7482'
      },
      boxShadow: {
        'select-shadow': '-4px 4px 12px rgba(0, 0, 0, 0.06)',
        'form-shadow': '0px 4px 8px 8px rgba(0, 0, 0, 0.25)'
      },
      backgroundImage: {
        'date-icon': "url('../src/assets/input-icons/date-icon.svg')"
      }
    }
  },
  plugins: []
}
