const swiper = new Swiper('.swiper', {
  // Optional parameters
  direction: 'horizontal',
  loop: false,

  // If we need pagination
  pagination: {
    el: '.swiper-pagination',
  },

  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },

  // And if we need scrollbar
  scrollbar: {
    el: '.swiper-scrollbar',
  },
  spaceBetween: 30,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
});

const arrowLeft = document.querySelector("[data-slider='arrow-left']");
const arrowRight = document.querySelector("[data-slider='arrow-right']");

arrowLeft.addEventListener('click', () => swiper.slidePrev());
arrowRight.addEventListener('click', () => swiper.slideNext());


const updateArrowsState = () => {
  swiper.activeIndex === 0 ? arrowLeft.setAttribute("disabled", "true") : arrowLeft.removeAttribute("disabled")
  swiper.activeIndex === swiper.slides.length - 1 ? arrowRight.setAttribute("disabled", "true") : arrowRight.removeAttribute("disabled")
}

updateArrowsState()
swiper.on('activeIndexChange', updateArrowsState);
