/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      fontFamily: {
        'Montserrat': ['Montserrat'],
        'MontserratALT': ['Montserrat Alternates'],
        'Nunito': ['Nunito'],
      },
      colors: {
        'button-blue': '#6347EB',
        'header-link': '#666571',
        'active-link': '#FE7244',
        'title': '#3D3D3D',
        'title--insert': '#FE7244',
        'description': '#4E4D56',
        'teacher-sphere': 'rgba(160, 113, 255, 0.65)',
        'machine-sphere': 'rgba(99, 203, 254, 0.65)',
        'data-sphere': 'rgba(121, 226, 177, 0.65)',
        'mobile-sphere': 'rgba(249, 148, 98, 0.65)',
        'explore__button': '#6652DA',
        'read-more': '#4D4D56',
        'instructor__button': '#D8E1F6',
        'footer__links-title': '#24242E',
        'footer__link': '#3B3A41',

      },
      backgroundColor: {
        'learning-element-transperent-block': 'rgba(255,255,255,80%)',
        'decor-line': '#AA80FF',
        'browse-element--active': 'rgba(249, 148, 98, 0.80)',
        'best-seller': '#FFF2D9',
        'web-sphere': 'rgba(170, 128, 255, 0.12)',
        'machine-sphere': 'rgba(71, 194, 255, 0.12)',
        'data-sphere': 'rgba(121, 226, 138, 0.12)',
        'mobile-sphere': 'rgba(249, 148, 98, 0.12)',
        'explore__button': 'rgba(182, 169, 255, 0.15)',
        'footer-rights-bg': '#F5F5F5',


      },
      backgroundImage: {
        'instructor-bg': 'linear-gradient(to right, rgb(202, 216, 249) 0%, rgb(253, 254, 255) 87%)',
        'instructor-bg-img': "url('../../src/img/instructor/Instructor.png')",

      },
      boxShadow: {
        'box-shadow-learning-element': '4px 1px 22px 2px rgba(102, 82, 218, 0.1)',
        'box-shadow-browse-element': '0px 2px 20px rgba(31, 31, 51, 0.03)',
        'box-shadow-explore-categories': '0px 3px 24px rgba(0, 0, 0, 0.03)',
        'box-shadow-explore-card': '0px 8px 24px rgba(31, 31, 51, 0.06)',
      },
      container: {
        center: true,
        screens: {
          md: '1200px',
        }
      }
    },
  },
  plugins: [],
}
