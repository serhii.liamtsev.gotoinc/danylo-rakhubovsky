import { createApp } from 'vue'
import App from './App.vue'
import './index.css'
import router from './router'
import { makeServer } from './mock-api/server.js'

makeServer()

const app = createApp(App)
app.use(router)
app.mount('#app')
