export function saveData(server) {
  localStorage.setItem('allRequestsList', JSON.stringify(server.db.dump()))
}

export function loadData() {
  try {
    const data = localStorage.getItem('allRequestsList')

    return JSON.parse(data)
  } catch (_) {
    return null
  }
}
