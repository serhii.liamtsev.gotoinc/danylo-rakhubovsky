import { loadData } from './utils.js'

export function makeRequestsHandlers(server) {
  server.get('/api/requests', (schema) => {
    return loadData().requests
  })
  server.post('/api/requests', (schema, request) => {
    const USER_ID = localStorage.getItem('USER_ID')
    const allRequestsListToStorage = loadData()
    const form = JSON.parse(request.requestBody).data
    allRequestsListToStorage.requests.push({ USER_ID: +USER_ID, form })

    localStorage.setItem(
      'allRequestsList',
      JSON.stringify(allRequestsListToStorage),
    )
  })
}
