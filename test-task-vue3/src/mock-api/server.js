import { createServer, Model } from 'miragejs'
import { makeRequestsHandlers } from './requests.js'

import { saveData, loadData } from './utils.js'

export function makeServer() {
  const initialData = loadData()
  return createServer({
    models: {
      request: Model,
    },
    seeds(server) {
      if (initialData) {
        server.db.loadData(initialData)
      } else {
        server.create('request', {
          USER_ID: 0,
          form: {
            cityFrom: 'cityFrom11',
            cityTo: 'cityTo1',
            typeOfParcel: 'gadgets',
            date: '09.05.2022',
            desc: 'description1',
          },
        })
        server.create('request', {
          USER_ID: 0,
          form: {
            cityFrom: 'cityFrom11',
            cityTo: 'cityTo1',
            typeOfParcel: 'gadgets',
            date: '09.05.2022',
            desc: 'description1',
          },
        })
        server.create('request', {
          USER_ID: 1,
          form: {
            cityFrom: 'cityFrom11',
            cityTo: 'cityTo1',
            typeOfParcel: 'gadgets',
            date: '09.05.2022',
            desc: 'description1',
          },
        })
        saveData(server)
      }
    },
    routes() {
      makeRequestsHandlers(this)
    },
  })
}
