const swiper = new Swiper('.swiper', {
  // Optional parameters
  direction: 'horizontal',
  loop: false,

  // If we need pagination
  pagination: {
    el: '.swiper-pagination',
  },

  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },

  // And if we need scrollbar
  scrollbar: {
    el: '.swiper-scrollbar',
  },
  spaceBetween: 30,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
});

const arrowLeft = document.querySelector('.about__cards-slider-arrow-left');
const arrowRight = document.querySelector('.about__cards-slider-arrow-right');

arrowLeft.addEventListener('click', () => swiper.slidePrev());
arrowRight.addEventListener('click', () => swiper.slideNext());


const updateArrowsState = () =>{
  arrowLeft.classList.toggle('about__cards-slider-arrow--disabled', swiper.activeIndex === 0);
  arrowRight.classList.toggle('about__cards-slider-arrow--disabled', swiper.activeIndex === swiper.slides.length - 1);
}

updateArrowsState()
swiper.on('activeIndexChange', updateArrowsState);

